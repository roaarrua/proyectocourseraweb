$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
	   interval: 1800
    });
    $('#suscripcion').on('show.bs.modal', function(e){
	   console.log('El modal se está mostrando');
	   $('#suscripcionBtn').removeClass('btn-outline-success');
	   $('#suscripcionBtn').addClass('btn-secondary');
	   $('#suscripcionBtn').prop('disable', true);
    });
    $('#suscripcion').on('shown.bs.modal', function(e){
	   console.log('El modal se mostró');
    });
    $('#suscripcion').on('hide.bs.modal', function(e){
	   console.log('El modal se está ocultando');
	   $('#suscripcionBtn').removeClass('btn-secondary');
	   $('#suscripcionBtn').addClass('btn-outline-success');
	   $('#suscripcionBtn').prop('disable', false);
    });
    $('#suscripcion').on('hidden.bs.modal', function(e){
	   console.log('El modal se ocultó');
    });		
});